<?php

namespace App\Console\Commands;

use App\Repositories\HogeRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;


class RunQuery extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:run-query';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $hogeRepository;

    /**
     * Create a new command instance.
     *
     * @param HogeRepository $hogeRepository
     */
    public function __construct(HogeRepository $hogeRepository)
    {
        $this->hogeRepository = $hogeRepository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $from = hrtime(true);
        Log::info($this->hogeRepository->findById(1));
        $to = hrtime(true);
        Log::info($to - $from);

        $from = hrtime(true);
        Log::info($this->hogeRepository->findByIdFromAll(1));
        $to = hrtime(true);
        Log::info($to - $from);

        $from = hrtime(true);
        Log::info($this->hogeRepository->findByIdFromQuery(1));
        $to = hrtime(true);
        Log::info($to - $from);
    }
}
