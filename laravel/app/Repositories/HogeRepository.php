<?php


namespace App\Repositories;


use App\Hoge;

class HogeRepository
{
    function findById(int $id)
    {
        return Hoge::find($id);
    }

    function findByIdFromAll(int $id)
    {
        return Hoge::all()->find($id);
    }

    function findByIdFromQuery(int $id)
    {
        return Hoge::query()->find($id);
    }
}
